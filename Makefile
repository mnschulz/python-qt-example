.DEFAULT_GOAL := install

PATHS_TO_FORMAT=.

install:
	poetry install

lock:
	poetry lock --no-update

lint:
	mypy ${PATHS_TO_FORMAT}
	ruff check ${PATHS_TO_FORMAT}