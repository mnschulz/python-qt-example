# Python QT Example

Example Project created to show some application design concepts to beginners. Also check out the 
[IHB Developer Onboarding Guide](https://rochewiki.roche.com/confluence/display/IHBIT/Onboarding+Developer) 
on Confluence.

Feel free to copy this repository and adapt it to your own usecase (e.g. without QT). If you already know the 
concepts and install `cookiecutter` you can simply create a new repository with [this Repository Template](https://code.roche.com/code-den/repository-template)

> ⚠️ Depending on the network configuration, you might need to disable SSL verification: `conda config --set ssl_verify false`

> Note: This example, as well as the repository template will be merged in the future

Initializing the repository:

```
conda env create -f environment.yml
conda activate python-qt-example
poetry install
```

> Note: The creation of a conda environment might take longer the first time as everything is downloaded from scratch


## Reproducible Builds
Always aim for [Reproducible Builds](https://reproducible-builds.org/) that anyone can have the same Python environment as you with 1-2 
command line calls.

In our case we use [Conda](https://docs.conda.io/en/latest/) to describe the environment of packages needed to:
- Build the installation environment: Python, Pip and Poetry
- Other packages impossible to install via Python package managers

We then use [Poetry](https://python-poetry.org/) to manage the Python dependencies as it adapts the dependency files 
for others on the fly, including transivite dependencies: The dependencies of your dependencies

> Note: You only need to call `poetry install` once during development as the files are linked locally
> Note 2: Differentiate development dependencies from build dependencies (`poetry add -group dev <package>`)


## Development
We use poetry to manage packages, please check the [poetry basic usage](https://python-poetry.org/docs/basic-usage/) for more information.

We installed type checking and linting of common issues, you can run these checks with
```bash
make lint
```

This will use Python hints to check with [MyPy](https://mypy-lang.org/) for consistency and 
[Ruff](https://docs.astral.sh/ruff/) for common code quality checks.


## Command Line Tool
We install a CLI tool with `[tool.poetry.scripts]` creating the `qt-app` tool calleable within the 
conda environment, try it with the help flag!

To start the application with the provided example configuration, run:

```
qt-app gui start examples/config.cfg
```
