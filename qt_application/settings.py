import datetime
from pathlib import Path
from typing import Any

# define the log file path
log_dir: Path = Path("~/.qt_app_logs").expanduser()
log_dir.mkdir(exist_ok=True, parents=True)
log_file: Path = log_dir / f'info_{datetime.datetime.now().strftime("%Y%m%d_%H%M%S")}.log'  # noqa: DTZ005


# Global dictionary to store logging settings
# Example setting logs both to stdout as well as to a file at a hidden home location
LOGGING_CONFIG: dict[str, Any] = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "format": "%(asctime)s:%(name)s:%(process)d:%(lineno)d %(levelname)s %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
        "simple": {
            "format": "%(message)s",
        },
    },
    "handlers": {
        "verbose_output": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "default",
            "stream": "ext://sys.stdout",
        },
        "warning_output": {
            "class": "logging.FileHandler",
            "level": "INFO",
            "formatter": "default",
            "filename": log_file,
            "mode": "w",
        },
    },
    "root": {
        "level": "INFO",
        "handlers": ["verbose_output", "warning_output"],
    },
}
