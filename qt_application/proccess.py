import logging
import time
from enum import Enum
from logging.config import dictConfig

from PySide6.QtCore import QObject, QRunnable, Signal, Slot
from typing_extensions import Self

from qt_application.settings import LOGGING_CONFIG

# Configure logging
dictConfig(LOGGING_CONFIG)
logger: logging.Logger = logging.getLogger(__name__)


class CalculationState(Enum):
    starting = "Starting"
    in_progress = "In Progress..."
    finished = "Finished"


class WorkerSignals(QObject):
    """Custom signals can only be defined on objects derived from QObject.

    Since QRunnable is not derived from QObject we can't define the signals there directly.
    A custom QObject to hold the signals is the simplest solution.
    """

    finished = Signal(CalculationState)
    progress = Signal(int)


class Worker(QRunnable):
    execution_time: int
    signals: WorkerSignals

    def __init__(self: Self, execution_time: int) -> None:
        super(Worker, self).__init__()
        self.execution_time = execution_time
        self.signals = WorkerSignals()

    @Slot()
    def run(self: Self) -> None:
        logger.info("Starting calculation")
        self.signals.progress.emit(0)
        self.signals.finished.emit(CalculationState.starting)
        time.sleep(1)
        self.signals.finished.emit(CalculationState.in_progress)

        # Update scaled progress bar in updates of 10
        for i in range(1, 11):
            self.signals.progress.emit(i*10)
            time.sleep(float(self.execution_time/10.0))

        self.signals.finished.emit(CalculationState.finished)
