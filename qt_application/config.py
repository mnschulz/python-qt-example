from enum import Enum

from pydantic import BaseModel


class ConfigModel(BaseModel):
    """Base config class to use Enums as types."""

    class Config:
        use_enum_values = True

class Language(Enum):
    english = "english"
    french = "french"
    german = "german"


class GUIConfig(ConfigModel):
    language: Language
    execution_time: int
