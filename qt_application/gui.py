import configparser
import logging
import sys
from logging.config import dictConfig
from pathlib import Path

import click
from PySide6.QtCore import Qt, QThreadPool
from PySide6.QtWidgets import QApplication, QLabel, QProgressBar, QPushButton, QVBoxLayout, QWidget
from typing_extensions import Self

from qt_application.config import GUIConfig, Language
from qt_application.proccess import CalculationState, Worker
from qt_application.settings import LOGGING_CONFIG

# Note: In principle snake_case would be nicer but leads to problems in the IDE
# from __feature__ import snake_case, true_property

# Configure logging
dictConfig(LOGGING_CONFIG)
logger: logging.Logger = logging.getLogger(__name__)

# Predefined language prompts
HELLO_TEXT: dict[Language, str] = {
    Language.english: "Hello World",
    Language.french: "Bonjour le monde",
    Language.german: "Hallo Welt",
}


SUCCESS_TEST: dict[Language, str] = {
    Language.english: "Success!",
    Language.french: "Succès!",
    Language.german: "Erfolg!",
}


# GUI Widget
class MainWindow(QWidget):
    gui_config: GUIConfig

    def __init__(
        self: Self,
        config_file_path: str,
    ) -> None:
        QWidget.__init__(self)
        # First parse config
        self._parse_config_file(config_file_path)

        self.hello_text: str = HELLO_TEXT[Language(self.gui_config.language)]
        self.success_text: str = SUCCESS_TEST[Language(self.gui_config.language)]

        # Set up general QT widgets
        self.layout = QVBoxLayout(self)

        self.hello_message = QLabel(self.hello_text)
        self.hello_message.alignment = Qt.AlignCenter
        self.layout.addWidget(self.hello_message)

        # Set up calculation widgets
        self.button = QPushButton("Start Calculation")
        self.button.clicked.connect(self.start_calculation)
        self.layout.addWidget(self.button)

        self.progress_bar = QProgressBar()
        self.progress_bar.setRange(0, 100)
        self.progress_bar.setValue(0)
        self.layout.addWidget(self.progress_bar)

        self.calculation_result = QLabel("Calculations not started yet")
        self.layout.addWidget(self.calculation_result)

        # Setup thread tpool (can be used for multi-threading if desired)
        self.threadpool = QThreadPool()
        logger.info(f"Multithreading with maximum {self.threadpool.maxThreadCount()} threads")

    def _parse_config_file(self: Self, config_file_path: str) -> None:
        # Get data from config file
        config = configparser.ConfigParser()
        config_file = Path(config_file_path)

        if not config_file.exists():
            raise FileNotFoundError(f"Config file {config_file_path} does not exist")

        with config_file.open() as fd:
            config.read_file(fd)

        logger.info(f"Config file {config_file_path} found")

        # Parse config file
        self.gui_config = GUIConfig(
            **config["GUI"],
        )
        logger.info(f"Config file {config_file_path} parsed, config: {self.gui_config}")

        if self.gui_config.execution_time < 1:
            raise ValueError(
                f"Execution time must be positive, but is {self.gui_config.execution_time}",
            )

    def start_calculation(self: Self) -> None:
        worker = Worker(self.gui_config.execution_time)
        worker.signals.finished.connect(self.update_status)
        worker.signals.progress.connect(self.update_progress)
        self.threadpool.start(worker)
        logger.info("Worker is in the queue doing its job")

    def update_status(self: Self, status: CalculationState) -> None:
        if status == CalculationState.finished:
            self.button.setEnabled(True)
            self.calculation_result.setText(self.success_text)
        else:
            self.button.setEnabled(False)
            self.calculation_result.setText(status.value)
        logger.info(f"Status updated to {status}")

    def update_progress(self: Self, progress: int) -> None:
        self.progress_bar.setValue(progress)


# Command to run from command line
@click.group()
def gui() -> None:
    """Commands starting the GUI."""
    pass


@gui.command()
@click.argument(
    "config_file_path",
    nargs=1,
    required=True,
)
def start(config_file_path: str) -> None:
    """Starts the GUI with the given config file."""
    logger.info(f"Starting GUI with config file {config_file_path}")

    app = QApplication(sys.argv)

    try:
        widget = MainWindow(
            config_file_path,
        )
        widget.show()

        sys.exit(app.exec())
    except Exception:
        logger.exception(f"Error starting GUI with config file {config_file_path}")
        raise
