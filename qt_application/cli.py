"""CLI of the storage tools."""
import click

from qt_application import __version__
from qt_application.gui import gui


@click.group("python-qt-example", context_settings={"help_option_names": ["-h", "--help"]})
@click.version_option(__version__)
def cli() -> None:
    """Top level command for all Storage Tools commands."""
    pass


cli.add_command(gui)
