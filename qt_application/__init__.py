import pkg_resources

# Add version constant to expose in GUI and CLI
__version__ = pkg_resources.get_distribution("python-qt-example").version
